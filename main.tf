terraform {
  backend "http" {
  }
}

provider "aws" {
  region = "eu-central-1"
}

resource "aws_iam_user" "gitlab" {
  name = "gitlab-deploy"
  path = "/"

  tags = {
    managed = "terraform"
  }
}

resource "aws_iam_user" "gitlab_prod" {
  name = "gitlab-deploy-prod"
  path = "/"

  tags = {
    managed = "terraform"
  }
}

module "ecr" {
  source    = "git::https://github.com/cloudposse/terraform-aws-ecr.git?ref=master"
  namespace = "gitlab"
  stage     = "prod"
  name      = "ecr"
  principals_full_access = [
    aws_iam_user.gitlab.arn,
    aws_iam_user.gitlab_prod.arn
  ]
}
